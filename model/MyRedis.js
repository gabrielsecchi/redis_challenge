let MSG_OK = "OK";
let MSG_ERROR = "(error) ";
let MSG_ERROR_TYPE = "WRONGTYPE Operation against a key holding the wrong kind of value";
let MSG_NIL = "(nil)";
let MSG_SIZE = "(size) ";
let MSG_INT = "(integer) ";
let MSG_EMPTY_LIST = "(empty list or set)";

let TYPE_KEY_VALUE = "KEY-VALUE";
let TYPE_RANGE = "KEY-RANGE";

class MyRedis {

    constructor(db) {
        this._db = db;
    }

    set(key, value, timer = undefined){
        try {
            if( this._db[key] !== undefined && this.isTypeRange(key) ) {
                throw MSG_ERROR_TYPE;
            }

            if(timer !== undefined) {
                var int_timer = parseInt(timer);
                if(isNaN(int_timer)) {
                    throw "this timer is not an integer";
                }

                setTimeout(
                    function(_class, key) {
                        _class.del(key);
                    },
                    (int_timer * 1000),
                    this,
                    key
                );
            }

            this._db[key] = {
                value : value,
                type : TYPE_KEY_VALUE
            }

            return MSG_OK;
        } catch (e) {
            return MSG_ERROR + e;
        }
    }

    zadd(key, score, member ) {
        try {
            if(this._db[key] !== undefined && ! this.isTypeRange(key) ) {
                throw MSG_ERROR_TYPE;
            }

            var floatScore = parseFloat(score);
            if( isNaN(floatScore) ) {
                throw MSG_ERROR + "score is not a valid float";
            }

            if(this._db[key] === undefined) {
                this._db[key] = {
                    value : [],
                    type : TYPE_RANGE
                };
            }

            this._db[key].value[member] = floatScore;

            return MSG_OK
        } catch (e) {
            return e;
        }
    }

    zrange(key, start, stop, withScore = false) {
        try {
            start = parseInt(start);
            stop = parseInt(stop);

            if(isNaN(start) || isNaN(stop)) {
                throw "value is not an integer";
            }

            if(this._db[key] === undefined) {
                return MSG_EMPTY_LIST;
            }

            if( ! this.isTypeRange(key) ) {
                throw MSG_ERROR_TYPE;
            }

            var result = new Array();
            var count = 0;
            var tempValues = this._db[key].value;
            var valueKeys = Object.keys(this._db[key].value);
            var valuesSliced;
            if(stop === -1) {
                valuesSliced = valueKeys.slice(start);
            }
            else {
                if(stop < -1) {
                    stop++;
                }
                valuesSliced = valueKeys.slice(start, stop);
            }

            valuesSliced.forEach(function (member, index) {
                if(!withScore) {
                    count = index;
                }

                count++;
                var text = count + ") " + member;
                result.push(text);

                if(withScore){
                    count++;
                    text = count + ") " + tempValues[member];
                    result.push(text);
                }
            });

            if(result.length === 0) {
                return MSG_EMPTY_LIST;
            }

            return result.join("\r\n");
        } catch (e) {
            return MSG_ERROR + e;
        }
    }

    zrank(key, member) {
        try {
            if(this._db[key] === undefined) {
                return MSG_NIL;
            }

            if( ! this.isTypeRange(key) ) {
                throw MSG_ERROR_TYPE;
            }

            var valueKeys = Object.keys(this._db[key].value);
            var result = valueKeys.indexOf(member);
            if(result < 0) {
                result = MSG_NIL;
            }
            return result
        } catch (e) {
            return MSG_ERROR + e;
        }
    }

    zcard(key) {
        if(this._db[key] === undefined) {
            return 0;
        }

        if( ! this.isTypeRange(key) ) {
            return MSG_ERROR + MSG_ERROR_TYPE;
        }

        return Object.keys(this._db[key].value).length;
    }

    get(key){
        if(key in this._db){
            if( this.isTypeRange(key) ) {
                return MSG_ERROR_TYPE;
            }

            return this._db[key].value;
        }

        return MSG_NIL;
    }

    dbSize() {
        return MSG_SIZE + Object.keys(this._db).length;
    }

    incr(key){
        if(key in this._db){
            if( this.isTypeRange(key) ) {
                return MSG_ERROR_TYPE;
            }

            var int_value = parseInt(this._db[key].value);
            if ( isNaN(int_value) ) {
                return MSG_ERROR + "value is not an integer";
            }

            this._db[key].value = int_value + 1;
            return MSG_INT + this._db[key].value;
        }

        this.set(key, 1);
        return MSG_INT + this._db[key].value;
    }

    del(key){
        if(key in this._db) {
            delete this._db[key];
            return MSG_OK;
        }

        return MSG_NIL;
    }

    isTypeRange(key) {
        return this._db[key].type === TYPE_RANGE;
    }
};

module.exports = MyRedis;
