var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var MyRedis  = require('../model/MyRedis')
var DB = [];

var app = express();
app.MyRedis = new MyRedis(DB);

/* configure middleware body-parser */
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/*  configure autoload of routes and controllers to app */
consign()
	.include('routes')
	.then('controllers')
	.into(app);

app.use(function(req, response, next){
	response.status(404).json({error: {code: 404, description: "Not found"}});
	next();
});

module.exports = app;
