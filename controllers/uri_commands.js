var get = function(application, request, response){
	var params = request.query.cmd.split(" ");
	var command = params[0];

	switch (command.toUpperCase()) {
		case 'SET':
			request.params.key = params[1];
			request.body[params[1]] = params[2];
			if(params.length >= 4 && params[3].toUpperCase() == "EX") {
				request.body.EX = params[4];
			}
			application.controllers.put.set(application, request, response);
			break;
		case 'INCR':
			request.params.key = params[1];
			application.controllers.put.incr(application, request, response);
			break;
		case 'ZADD':
			request.params.key = params[1];
			request.body.score = params[2];
			request.body.member = params[3];
			application.controllers.put.zadd(application, request, response);
			break;
		case 'DEL':
			request.params.key = params[1];
			application.controllers.delete.delete(application, request, response);
			break;
		case 'GET':
			request.params.key = params[1];
			application.controllers.get.get(application, request, response);
			break;
		case 'DBSIZE':
			application.controllers.get.dbsize(application, request, response);
			break;
		case 'ZCARD':
			request.params.key = params[1];
			application.controllers.get.zcard(application, request, response);
			break;
		case 'ZRANK':
			request.params.key = params[1];
			request.params.member = params[2];
			application.controllers.get.zrank(application, request, response);
			break;
		case 'ZRANGE':
			request.params.key = params[1];
			request.params.start = params[2];
			request.params.stop = params[3];
			application.controllers.get.zrange(application, request, response);
			break;
		default:
			response.status(500).json("Invalid command");
	}

}

module.exports = {
	get: get
}
