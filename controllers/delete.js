var del = function(application, request, response){
	var key = request.params.key;

	response.status(200).json(
		application.MyRedis.del(key)
	);
}

module.exports = {
	delete: del
}
