var get = function(application, request, response){
	var key = request.params.key;

	response.status(200).json(
		application.MyRedis.get(key)
	);
}

var dbsize = function(application, request, response){

	response.status(200).json(
		application.MyRedis.dbSize()
	);
}

var zcard = function(application, request, response){
	var key = request.params.key;

	response.status(200).json(
		application.MyRedis.zcard(key)
	);
}

var zrank = function(application, request, response){
	var key = request.params.key;
	var member = request.params.member;

	response.status(200).json(
		application.MyRedis.zrank(key, member)
	);
}

var zrange = function(application, request, response){
	var key = request.params.key;
	var start = request.params.start;
	var stop = request.params.stop;
	var withScore = (request.params.withScore || false);

	if( withScore === "WITHSCORE" || withScore === false  ) {
		if(withScore === "WITHSCORE") {
			withScore = true;
		}

		response.status(200).send(
			application.MyRedis.zrange(key, start, stop, withScore)
		);
		return;
	}

	response.status(200).json(
		"WITHSCORE parameter must be 'WITHSCORE' or not sent"
	);
}

module.exports = {
	get: get,
	dbsize: dbsize,
	zcard: zcard,
	zrank: zrank,
	zrange: zrange
}
