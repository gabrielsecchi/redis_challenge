var set = function(application, request, response){
	var key = request.params.key;
	var value = request.body[key] || Object.keys(request.body)[0];
	var expireMode = ('ex' in request.body || 'EX' in request.body);

	var timer = undefined;
	if(expireMode){
		timer = (request.body.ex || request.body.EX);
	}

	response.status(200).json(
		application.MyRedis.set(key, value, timer)
	);
}

var incr = function(application, request, response){
	var key = request.params.key;

	response.status(200).json(
		application.MyRedis.incr(key)
	);
}

var zadd = function(application, request, response){
	var key = request.params.key;
	var score = request.body.score || Object.keys(request.body)[0];
	var member = request.body.member || Object.keys(request.body)[1];

	response.status(200).json(
		application.MyRedis.zadd(key, score, member)
	);
}

module.exports = {
	set: set,
	incr: incr,
	zadd: zadd
}
