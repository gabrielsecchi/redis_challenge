module.exports = function(application){
	application.put('/incr/:key', function(request, response){
		application.controllers.put.incr(application, request, response);
	});

	application.put('/zadd/:key', function(request, response){
		application.controllers.put.zadd(application, request, response);
	});

	application.put('/:key', function(request, response){
		application.controllers.put.set(application, request, response);
	});

	application.get('/dbsize', function(request, response){
		application.controllers.get.dbsize(application, request, response);
	});

	application.get('/zcard/:key', function(request, response){
		application.controllers.get.zcard(application, request, response);
	});

	application.get('/zrank/:key/:member', function(request, response){
		application.controllers.get.zrank(application, request, response);
	});

	application.get('/zrange/:key/:start/:stop/:withScore?', function(request, response){
		application.controllers.get.zrange(application, request, response);
	});

	application.get('/:key', function(request, response){
		application.controllers.get.get(application, request, response);
	});

	application.delete('/:key', function(request, response){
		application.controllers.delete.delete(application, request, response);
	});

	application.get('/', function(request, response){
		application.controllers.uri_commands.get(application, request, response);
	});
}
